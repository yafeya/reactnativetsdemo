import { createStackNavigator } from 'react-navigation';
import Home from './src/components/home/Home';
import Friends from './src/components/friends/Friends';

const AppNavigator = createStackNavigator(
  {
    Home: Home,
    Friends: Friends
  },
  {
    initialRouteName: 'Home',
  });

export default AppNavigator;
