/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 * 
 * @format
 */

import React from 'react';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import promise from 'redux-promise-middleware';
import { createAppContainer } from 'react-navigation';
import AppNavigator from './AppNavigator';
import { homeReducer } from './src/components/home/HomeReducer';


export default class App extends React.Component {
  render() {
    const middleware = [promise]
    const store = createStore(appReducer, applyMiddleware(...middleware));
    const AppContainer = createAppContainer(AppNavigator);

    return (
      <Provider store={ store }>
        <AppContainer />
      </Provider>
    );
  }
}

const appReducer = combineReducers({
  home: homeReducer
});