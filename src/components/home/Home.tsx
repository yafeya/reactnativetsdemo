import React, { Dispatch } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { connect } from 'react-redux';
import { State } from '../../state/State';
import { HomeState } from '../../state/HomeState';
import { bindActionCreators } from 'redux';
import { setTitle } from './HomeActions';
import { VoidTypeAnnotation } from '@babel/types';

interface HomeProps {
  home?: HomeState;
  navigation: any;
  setTitle(title: string): void;
}

class Home extends React.Component<HomeProps> {

  constructor(props: HomeProps) {
    super(props);
  }

  componentDidMount() {
    this.props.setTitle('This is a Home');
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>{this.props.home ? this.props.home.title : 'HOME!!!'}</Text>
        <Button title="To Friends" onPress={() => { this.props.navigation.navigate('Friends');}}></Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapStateToProps = (state: State) => {
  const { home } = state
  return { home }
};

const mapDispatchToProps = (dispatch: any) => ({ 
  setTitle: (title: string) => dispatch(setTitle(title))
})

export default connect(mapStateToProps, mapDispatchToProps)(Home);