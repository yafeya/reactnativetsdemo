export interface SetTitleAction { 
    type: string;
    payload: string;
}

export const SET_TITLE = 'SET_TITLE';

export type HomeActions = SetTitleAction;

export function setTitle(title: string): SetTitleAction {
    return {
        type: SET_TITLE,
        payload: title
    };
}