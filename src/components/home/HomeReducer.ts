import { HomeState } from "../../state/HomeState";
import { SetTitleAction, HomeActions, SET_TITLE } from "./HomeActions";

const initialState: HomeState = {
    title: ''
};

export function homeReducer(state = initialState, action: HomeActions): HomeState {
    switch (action.type) {
        case SET_TITLE:
            return {
                ...state,
                title: action.payload
            };
        default:
            return state;
    }
}