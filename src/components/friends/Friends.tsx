import React, { Dispatch } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { connect } from 'react-redux';
import { State } from '../../state/State';
import { HomeState } from '../../state/HomeState';
import { bindActionCreators } from 'redux';
import { VoidTypeAnnotation } from '@babel/types';

class Friends extends React.Component {


  render() {
    return (
      <View style={styles.container}>
        <Text>Friends Page</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default connect()(Friends);